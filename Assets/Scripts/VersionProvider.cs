﻿// #
// # Created by Sercan Degirmenci on 2015.06.19
// #
using UnityEngine;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;

[InitializeOnLoad]
#endif
public sealed class VersionProvider : ScriptableObject
{
    private const string versionSettingsAssetName = "Version";
    private const string versionSettingsPath = "Resources";
    private const string versionSettingsAssetExtension = ".asset";

    [SerializeField] private string m_Commit = "none-auto";

    [SerializeField] private int m_BuildNumber = 1;


    public string Commit
    {
        get { return m_Commit; }
        set
        {
            m_Commit = value;
            DirtyEditor();
        }
    }

    public int BuildNumber
    {
        get { return m_BuildNumber; }
        set
        {
            m_BuildNumber = value;
            DirtyEditor();
        }
    }

    private static VersionProvider instance;

    public static VersionProvider Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load(versionSettingsAssetName) as VersionProvider;
                if (instance == null)
                {
                    // If not found, autocreate the asset object.
                    instance = CreateInstance<VersionProvider>();
#if UNITY_EDITOR
                    string properPath = Path.Combine(Application.dataPath, versionSettingsPath);
                    if (!Directory.Exists(properPath))
                    {
                        AssetDatabase.CreateFolder("", "Resources");
                    }

                    string fullPath = Path.Combine(Path.Combine("Assets", versionSettingsPath),
                        versionSettingsAssetName + versionSettingsAssetExtension);
                    AssetDatabase.CreateAsset(instance, fullPath);
#endif
                }
            }
            return instance;
        }
    }

#if UNITY_EDITOR
    [MenuItem("File/Edit Version")]
    public static void Edit() { Selection.activeObject = Instance; }

#endif

    public static void DirtyEditor()
    {
#if UNITY_EDITOR
        EditorUtility.SetDirty(Instance);
#endif
    }
}
