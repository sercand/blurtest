///////////////////////////////////////////////////////////////
// Copyright - 2014 Panteon
// Project Name: Legendary Team
// File Name   : Console.cs
// Author      : Ufuk
// Created On  : 21/1/2014 20:31
///////////////////////////////////////////////////////////////

using System.Collections;
using System.Linq;
using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// A console that displays the contents of Unity's debug log.
/// </summary>
public class PanConsole : MonoBehaviour
{
//#if UNITY_EDITOR || UNITY_STANDALONE_WIN

    public static readonly Version version = new Version(1, 0);

    struct ConsoleMessage
    {
        public readonly string message;
        public readonly string stackTrace;
        public readonly LogType type;

        public ConsoleMessage(string message, string stackTrace, LogType type)
        {
            this.message = message;
            this.stackTrace = stackTrace;
            this.type = type;
        }
    }

    public KeyCode toggleKey = KeyCode.BackQuote;
    public int logLimit = 100;

    static List<ConsoleMessage> entries = new List<ConsoleMessage>();

    Vector2 scrollPos;
    static bool show;
    bool autoScroll = true;

    // Visual elements:

    const int margin = 20;
    Rect windowRect = new Rect(margin, margin, Screen.width - (2 * margin), Screen.height - (2 * margin));
    GUIContent clearLabel = new GUIContent("Clear", "Clear the contents of the console.");
    GUIStyle opaqueBgStyle;

    private bool isFiltered = false;
    private static PanConsole Instance;

    private void Awake()
    {
#if UNITY_EDITOR
        gameObject.SetActive(false); // do not run on the editor
#endif
        //Check if instance already exists
        if (Instance == null)
            Instance = this; //if not, set instance to this
        else if (Instance != this) //If instance already exists and it's not this:
            Destroy(gameObject);
        //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
        DontDestroyOnLoad(gameObject); //Sets this to not be destroyed when reloading scene
        opaqueBgStyle = new GUIStyle
        {
            normal = {background = MakeTex(600, 1, new Color(0.0f, 0.0f, 0.0f, 0.4f))}
        };
    }

    private void OnEnable()
    {
        Debug.Log("OnEnable PanConsole");
        if (Instance == this)
        {
#if UNITY_5
            Application.logMessageReceived += HandleLog;
#else
            Application.RegisterLogCallback(HandleLog);
#endif
        }
       Debug.Log("Version: " + VersionProvider.Instance.BuildNumber + " " + VersionProvider.Instance.Commit);
    }

    private void OnDisable()
    {
        Debug.Log("Disable PanConsole");
        if (Instance == this)
        {
#if UNITY_5
            Application.logMessageReceived -= HandleLog;
#else
            Application.RegisterLogCallback(null);
#endif
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(toggleKey))
        {
            show = !show;
        }
    }

    void OnGUI()
    {
        int fontSize = Mathf.CeilToInt(Screen.height * 0.02f);

        GUI.skin.label.fontSize = fontSize;
        GUI.skin.button.fontSize = fontSize;
        GUI.skin.toggle.fontSize = fontSize;
        GUI.skin.button.fixedHeight = Screen.height * 0.06f;

        if (GUI.Button(new Rect(Screen.width - (Screen.height / 15f), Screen.height - (Screen.height / 15f), Screen.height / 15f, Screen.height / 15f), "C"))
        {
            show = !show;
        }

        if (!show)
        {
            return;
        }
        GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.04f;
        GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.04f;
        windowRect = GUILayout.Window(123456, windowRect, ConsoleWindow, "Console", opaqueBgStyle);
    }

    /// <summary>
    /// A window displaying the logged messages.
    /// </summary>
    /// <param name="windowID">The window's ID.</param>
    void ConsoleWindow(int windowID)
    {
        scrollPos = GUILayout.BeginScrollView(scrollPos);

        List<ConsoleMessage> entriesWillBeShown = (isFiltered) ? entries.Where(e => e.message.StartsWith("->") || e.message.StartsWith("<-")).ToList() : entries;

        // Go through each logged entry
        for (int i = 0; i < entriesWillBeShown.Count; i++)
        {
            ConsoleMessage entry = entriesWillBeShown[i];

            // If this message is the same as the last one and the collapse feature is chosen, skip it
            if (i > 0 && entry.message == entriesWillBeShown[i - 1].message)
            {
                continue;
            }

            string message = entry.message;

            // Change the text colour according to the log type
            switch (entry.type)
            {
                case LogType.Error:
                    GUI.contentColor = Color.red;
                    break;

                case LogType.Exception:
                    GUI.contentColor = Color.red;
                    message += entry.stackTrace;
                    break;
                case LogType.Warning:
                    GUI.contentColor = Color.yellow;
                    break;
                default:
                    GUI.contentColor = Color.white;
                    break;
            }

            if (entry.message.StartsWith("->"))
            {
                GUI.contentColor = Color.cyan;
            }
            else if (entry.message.StartsWith("<- "))
            {
                GUI.contentColor = Color.magenta;
            }

            GUILayout.Label(message);

            if (autoScroll)
                scrollPos += new Vector2(scrollPos.x, scrollPos.y + 20);
        }

        GUI.contentColor = Color.white;

        GUILayout.EndScrollView();

        GUILayout.BeginHorizontal();

        if (autoScroll)
        {
            if (GUILayout.Button("Manual Scroll"))
            {
                autoScroll = false;
            }
        }
        else
        {
            if (GUILayout.Button("Auto Scroll"))
            {
                autoScroll = true;
            }
        }

        // Clear button
        if (GUILayout.Button(clearLabel))
        {
            entries.Clear();
        }

        if (isFiltered)
        {
            if (GUILayout.Button("Unfilter"))
            {
                isFiltered = false;
            }
        }
        else
        {
            if (GUILayout.Button("Filter"))
            {
                isFiltered = true;
            }
        }

        if (GUILayout.Button("Close"))
        {
            show = false;
        }

        GUILayout.EndHorizontal();

        // Set the window to be draggable by the top title bar
        GUI.DragWindow(new Rect(0, 0, 10000, 20));
    }

    /// <summary>
    /// Logged messages are sent through this callback function.
    /// </summary>
    /// <param name="message">The message itself.</param>
    /// <param name="stackTrace">A trace of where the message came from.</param>
    /// <param name="type">The type of message: error/exception, warning, or assert.</param>
    void HandleLog(string message, string stackTrace, LogType type)
    {

        ConsoleMessage entry = new ConsoleMessage(message, stackTrace, type);
        if (entries.Count > logLimit)
        {
            entries.RemoveAt(0);
        }
        entries.Add(entry);
    }

    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];

        for (int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }
//#endif
}
