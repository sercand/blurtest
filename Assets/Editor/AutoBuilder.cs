﻿/* 
AutoBuilder.cs
Automatically changes the target platform and creates a build.
 
Installation
Place in an Editor folder.
 
Usage
Go to File > AutoBuilder and select a platform. These methods can also be run from the Unity command line using -executeMethod AutoBuilder.MethodName.
 
License
Copyright (C) 2011 by Thinksquirrel Software, LLC
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using System.IO;
using UnityEngine;

public static class AutoBuilder
{
    private const string projectName = "blurtest";
    private const string projectKey = "TD";

    private const string iosBuildFolder = "builds/ios";
    private const string androidBuildFolder = "builds/android";
    private const string DateFormat = "yy-MM-dd-HH-mm";
    private const string buildBumber = "-buildNumber";
#if UNITY_EDITOR_WIN
    private const string EnvironmentTextFile = "c:/Users/env.txt";
#else
        private const string EnvironmentTextFile = "/Users/Shared/env.txt";
#endif
    private static string AndroidSdkRoot { get; set; }

    private static string KeyStorePath { get; set; }

    private static string KeyStorePass { get; set; }

    private static string KeyStoreAliasPass { get; set; }

    private static string KeyStoreAlias
    {
        get { return "turklokumu"; }
    }

    private static string[] GetScenes()
    {
        return new[]
        {
            "Assets/Scene.unity",
        };
    }
    [MenuItem("File/AutoBuilder/iOS")]
    public static void ios()
    {
        init();
#if UNITY_5
        BuildTarget target = BuildTarget.iOS;
#else
        BuildTarget target = BuildTarget.iPhone;
#endif
        var folder = Path.Combine(Directory.GetParent(Application.dataPath).FullName, iosBuildFolder);
        if (!Directory.Exists(folder))
        {
            Directory.CreateDirectory(folder);
        }
        if (EditorUserBuildSettings.activeBuildTarget != target)
            EditorUserBuildSettings.SwitchActiveBuildTarget(target);
        
        var options = BuildOptions.None;
        try
        {
            var dev = CommandLineReader.GetCustomArgument("dev");

            if (dev == "true" || dev == "1")
            {
                options = BuildOptions.Development | BuildOptions.ConnectWithProfiler;
            }

            Debug.Log("Is it development build " + dev + " " + options);
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }

        Debug.Log("Build location: " + EditorUserBuildSettings.GetBuildLocation(target));
        var t = BuildPipeline.BuildPlayer(GetScenes(), folder, target, options);

        if (!string.IsNullOrEmpty(t))
        {
            EditorApplication.Exit(2);
        }
    }

    [MenuItem("File/AutoBuilder/Android")]
    public static void android()
    {
        init();
        EditorPrefs.SetString("AndroidSdkRoot", AndroidSdkRoot);

        PlayerSettings.Android.keystoreName = KeyStorePath;
        PlayerSettings.Android.keyaliasName = KeyStoreAlias;
        PlayerSettings.Android.keystorePass = KeyStorePass;
        PlayerSettings.Android.keyaliasPass = KeyStoreAliasPass;

        var name = GetApkName();
        var buildPath = string.Format("{0}/{1}.apk", androidBuildFolder, name);

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);

        BuildPipeline.BuildPlayer(GetScenes(), buildPath, BuildTarget.Android, BuildOptions.None);
    }
    private static void init()
    {
        try
        {
            var commit = CommandLineReader.GetCustomArgument("commit");
            var build = CommandLineReader.GetCustomArgument("build");
            VersionProvider.Instance.BuildNumber = Int32.Parse(build);
            VersionProvider.Instance.Commit = commit;
            Debug.Log("Building commit:" + commit + " and buildNumber: " + build);
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }
        const string storePathName = "KEY_STORE_" + projectKey + "_PATH";
        const string storePassName = "KEY_STORE_" + projectKey + "_PASS";
        const string storeAliasPassName = "KEY_STORE_" + projectKey + "_ALIAS_PASS";
        if (File.Exists(EnvironmentTextFile))
        {
            var text = File.ReadAllLines(EnvironmentTextFile);
            foreach (var s in text)
            {
                var s2 = s.Split('=');
                if (s2.Length != 2) continue;
                switch (s2[0])
                {
                    case "ANDROID_HOME":
                        AndroidSdkRoot = s2[1];
                        break;
                    case storePathName:
                        KeyStorePath = s2[1];
                        break;
                    case storePassName:
                        KeyStorePass = s2[1];
                        break;
                    case storeAliasPassName:
                        KeyStoreAliasPass = s2[1];
                        break;
                }
            }
        }
        else
        {
            AndroidSdkRoot = EditorPrefs.GetString("AndroidSdkRoot");
        }
    }

    #region Utils

    private class BuildFile
    {
        public string Path;
        public DateTime Time;
    }

    private static string GetApkName()
    {
        var args = Environment.GetCommandLineArgs();
        for (var i = 0; i < args.Length; i++)
        {
            if (args[i] == buildBumber)
            {
                return String.Format("{0}-{1}-{2}", projectName, DateTime.Now.ToString(DateFormat), args[i + 1]);
            }
        }
        return String.Format("{0}-{1}", projectName, DateTime.Now.ToString(DateFormat));
    }

    public static void DisableDsym(string buildFolder) { ProcessDebugInformation(buildFolder, "dwarf"); }

    public static void EnableDsym(string buildFolder) { ProcessDebugInformation(buildFolder, "dwarf-with-dsym"); }

    private static void ProcessDebugInformation(string buildFolder, string str)
    {
        Debug.Log("ProcessDebugInformation");
        var xcodeProjectPath = Path.Combine(buildFolder, "Unity-iPhone.xcodeproj");
        var pbxPath = Path.Combine(xcodeProjectPath, "project.pbxproj");

        var sb = new System.Text.StringBuilder();
        var xcodeProjectLines = File.ReadAllLines(pbxPath);

        int stage = 0;
        var subsection = "";

        foreach (var line2 in xcodeProjectLines)
        {
            if (line2.IndexOf("DEBUG_INFORMATION_FORMAT") < 0)
            {
                var line = line2;

              //  if (line.IndexOf("/* FbUnityInterface.mm */;") >= 0)
              //  {
              //      line = line.Replace("/* FbUnityInterface.mm */;", "/* FbUnityInterface.mm */; settings = {COMPILER_FLAGS = \'-fno-objc-arc\';};");
              //  }
              //  else 
                    if (line.IndexOf("/* JSONKit.m */;") >= 0)
                {
                    line = line.Replace("/* JSONKit.m */;",
                        "/* JSONKit.m */; settings = {COMPILER_FLAGS = \"-fno-objc-arc \"; };");
                }
              //  else if (line.IndexOf("/* KeychainItemWrapper.m */;") >= 0)
              //  {
              //      line = line.Replace("/* KeychainItemWrapper.m */;", "/* KeychainItemWrapper.m */; settings = {COMPILER_FLAGS = \'-fno-objc-arc\';};");
              //  }

                sb.AppendLine(line);
                if (stage == 0)
                {
                    if (line.IndexOf("/* Debug */ = ") >= 0)
                    {
                        subsection = "Debug";
                        stage = 2;
                    }
                    else if (line.IndexOf("/* Release */ = ") >= 0)
                    {
                        subsection = "Release";
                        stage = 2;
                    }
                }
                else if (stage == 2)
                {
                    if (line.IndexOf("buildSettings = {") >= 0)
                    {
                        if (subsection == "Debug")
                        {
                            sb.AppendLine(String.Format("\t\t\t\tDEBUG_INFORMATION_FORMAT = {0};", str));
                            stage = 0;
                        }
                        else if (subsection == "Release")
                        {
                            sb.AppendLine(String.Format("\t\t\t\tDEBUG_INFORMATION_FORMAT = {0};", str));
                            stage = 0;
                        }
                    }
                }
            }
        }

        File.Move(pbxPath, pbxPath + ".back");
        File.WriteAllText(pbxPath, sb.ToString());
    }

    #endregion
}

static class CommandLineReader
{
    //Config
    private const string CUSTOM_ARGS_PREFIX = "-Args:";
    private const char CUSTOM_ARGS_SEPARATOR = ';';

    public static string[] GetCommandLineArgs()
    {
        return Environment.GetCommandLineArgs();
    }

    public static string GetCommandLine()
    {
        string[] args = GetCommandLineArgs();

        if (args.Length > 0)
        {
            return string.Join(" ", args);
        }
        else
        {
            Debug.LogError("CommandLineReader.cs - GetCommandLine() - Can't find any command line arguments!");
            return "";
        }
    }

    public static Dictionary<string, string> GetCustomArguments()
    {
        Dictionary<string, string> customArgsDict = new Dictionary<string, string>();
        string[] commandLineArgs = GetCommandLineArgs();
        string[] customArgs;
        string[] customArgBuffer;
        string customArgsStr = "";

        try
        {
            customArgsStr = commandLineArgs.Where(row => row.Contains(CUSTOM_ARGS_PREFIX)).Single();
        }
        catch (Exception e)
        {
            Debug.LogError("CommandLineReader.cs - GetCustomArguments() - Can't retrieve any custom arguments in the command line [" + commandLineArgs + "]. Exception: " + e);
            return customArgsDict;
        }

        customArgsStr = customArgsStr.Replace(CUSTOM_ARGS_PREFIX, "");
        customArgs = customArgsStr.Split(CUSTOM_ARGS_SEPARATOR);

        foreach (string customArg in customArgs)
        {
            customArgBuffer = customArg.Split('=');
            if (customArgBuffer.Length == 2)
            {
                customArgsDict.Add(customArgBuffer[0], customArgBuffer[1]);
            }
            else
            {
                Debug.LogWarning("CommandLineReader.cs - GetCustomArguments() - The custom argument [" + customArg + "] seem to be malformed.");
            }
        }

        return customArgsDict;
    }

    public static string GetCustomArgument(string argumentName)
    {
        Dictionary<string, string> customArgsDict = GetCustomArguments();

        if (customArgsDict.ContainsKey(argumentName))
        {
            return customArgsDict[argumentName];
        }
        else
        {
            Debug.LogError("CommandLineReader.cs - GetCustomArgument() - Can't retrieve any custom argument named [" + argumentName + "] in the command line [" + GetCommandLine() + "].");
            return "";
        }
    }
}