﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

namespace Assets
{
    public class UIScripts:MonoBehaviour
    {
        [SerializeField]
        private BlurOptimized blur;
        [SerializeField]
        private Blur normalblur;
        [SerializeField]
        private Text downsampleText;
        [SerializeField]
        private Text blurSizeText;
        [SerializeField]
        private Text blurIterationText;

        public void DownsampleChanged(float v)
        {
            blur.downsample = (int) v;
        }
        public void BlurSizeChanged(float v)
        {
            blur.blurSize = v;
            normalblur.blurSpread = v;
        }
        public void BlurIterationsChanged(float v)
        {
            blur.blurIterations = (int)v;
            normalblur.iterations = (int)v;
        }

        public void Update()
        {
            downsampleText.text = blur.downsample.ToString();
            blurSizeText.text = blur.blurSize.ToString();
            blurIterationText.text = blur.blurIterations.ToString();
        }
    }
}